package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
        for(Contact c : rep.getContacts())
            rep.removeContact(c);

        try {
			con = new Contact("nume1", "Strada a", "+0709856325","email@test");
            rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		assertEquals(n,1);
	}
	
	@Test
	public void testCase2()
	{
        for(Contact c : rep.getContacts())
            rep.removeContact(c);

        try {
			con = new Contact("nume1", "", "+0709856325","email@test");
            rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase3()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);
		
		try {
			con = new Contact("nume1", "Strada c", "?","email@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

	}

	@Test
	public void testCase4()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("nume1", "Strada d", "1111111", "email1@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

	}

	@Test
	public void testCase5()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("M", "Adresa 1", "+0709856325", "email1@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
        int n  = rep.count();
        assertEquals(n,1);
	}

	@Test
	public void testCase6()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("M..1234", "Adresa 1", "+0709856325", "email1@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
        int n  = rep.count();
        assertEquals(n,1);
	}

	@Test
	public void testCase7()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("M..1234", "Adresa 1", "709856325", "email1@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

	}

	@Test
	public void testCase8()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("nume", "", "+0709856325", "email1@test");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

	}
	
}
