package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TopDownIntegrationTest {
    private Activity act;
    private RepositoryActivity rep;
    private Contact con;
    private RepositoryContact rep1;

    @Before
    public void setUp() throws Exception {
        rep = new RepositoryActivityMock();
        rep1 = new RepositoryContactMock();
    }

    public void testCase2()
    {
        for(Contact c : rep1.getContacts())
            rep1.removeContact(c);

        try {
            con = new Contact("nume1", "", "+0709856325","email@test");
            rep1.addContact(con);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }
    }

    public void testCase3()
    {
        testCase2();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try{
            for (Activity a : rep.getActivities())
                rep.removeActivity(a);

            act = new Activity("name1",
                    df.parse("03/20/2013 12:00"),
                    df.parse("03/20/2013 13:00"),
                    null,
                    "Lunch break");
            rep.addActivity(act);

            act = new Activity("name1",
                    df.parse("03/20/2013 12:30"),
                    df.parse("03/20/2013 13:30"),
                    null,
                    "Lunch break");
            assertFalse(rep.addActivity(act));
        }
        catch(Exception e){}
        assertTrue( 1 == rep.count());
        rep.saveActivities();
        for (Activity a : rep.getActivities())
            rep.removeActivity(a);
    }

    @Test
    public void testCase1() {
        testCase3();
        for(Contact c : rep1.getContacts())
            rep1.removeContact(c);

        try {
            con = new Contact("nume1", "", "+0709856325","email@test");
            rep1.addContact(con);
        } catch (InvalidFormatException e) {
            assertTrue(true);
        }

        for (Activity act : rep.getActivities())
            rep.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name1", start, end,
                new LinkedList<Contact>(), "description2");

        rep.addActivity(act);

        c.set(2013, 3 - 1, 20);

        List<Activity> result = rep.activitiesOnDate("name1", c.getTime());
        assertTrue(result.size() == 1);
    }

}
